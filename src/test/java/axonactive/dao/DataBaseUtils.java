package axonactive.dao;

import axonactive.core.SystemPropertiesCore;
import com.mongodb.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nvtien2 on 19/05/2016.
 */
public class DataBaseUtils {
    private static String mongoDBName;
    private static String mongoHost;
    private static Integer mongoPort;
    private static String mongoUser;
    private static String mongoPassword;
    private static MongoClient mongoClient = null;

    private static void connectToMongoDB() {
        mongoDBName = SystemPropertiesCore.getProperties().getProperty("MONGO_DATABASE_NAME");
        mongoHost = SystemPropertiesCore.getProperties().getProperty("MONGO_HOST");
        mongoPort = Integer.parseInt(SystemPropertiesCore.getProperties().getProperty("MONGO_PORT"));
        mongoUser = SystemPropertiesCore.getProperties().getProperty("MONGO_USER");
        mongoPassword = SystemPropertiesCore.getProperties().getProperty("MONGO_PASSWORD");

        List<ServerAddress> seeds = new ArrayList<ServerAddress>();
        seeds.add( new ServerAddress(mongoHost, mongoPort));
        List<MongoCredential> credentials = new ArrayList<MongoCredential>();
        if (!mongoUser.isEmpty() && !mongoPassword.isEmpty()) {
            credentials.add(MongoCredential.createCredential(mongoUser, mongoDBName, mongoPassword.toCharArray()));
        }
        mongoClient = new MongoClient( seeds, credentials );
    }
    public static MongoClient getMongoClient() {
        if (mongoClient == null) {
            connectToMongoDB();
        }
        return mongoClient;
    }

    public static DBCollection getCollection(String collectionName) {
        return getMongoClient().getDB(mongoDBName).getCollection(collectionName);
    }

    public static void insertDataToCollection(BasicDBObject data, String collectionName) {
        try {
            DBCollection collection = getCollection(collectionName);
            collection.save(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void clearDataWithAppId(String appId) {
        DBCollection collection;
        DBCursor dbCursor;
        try {
            collection = getCollection("session");
            dbCursor = collection.find(new BasicDBObject("app.id", appId));
            while (dbCursor.hasNext()) {
                DBObject object = dbCursor.next();
                collection.remove(object);
            }
            collection = getCollection("events");
            dbCursor = collection.find(new BasicDBObject("app.appId", appId));
            while (dbCursor.hasNext()) {
                DBObject object = dbCursor.next();
                collection.remove(object);
            }
            collection = getCollection("user");
            dbCursor = collection.find(new BasicDBObject("appId", appId));
            while (dbCursor.hasNext()) {
                DBObject object = dbCursor.next();
                collection.remove(object);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
