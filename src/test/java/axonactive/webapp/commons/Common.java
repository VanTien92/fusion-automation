package axonactive.webapp.commons;

import org.openqa.selenium.StaleElementReferenceException;

import java.util.NoSuchElementException;

import static org.junit.Assert.fail;

public class Common {

    public static void waitForElementToDisappear(String element, int timeout) throws InterruptedException{
        Boolean isElementNoExist = false;
        while (!isElementNoExist) {
            int t = 0;
            while (t < timeout) {
                try {
                    //SHOW
                   /* if (!driver.findElement(By.id(element)).isDisplayed()) {
                        isElementNoExist = true;
                        return;
                    }*/
                } catch ( StaleElementReferenceException ser ) {
                    isElementNoExist = false;
                } catch ( NoSuchElementException nse ) {
                    isElementNoExist = false;
                } catch ( Exception e ) {
                    isElementNoExist = false;
                    System.out.println("exception is not found out this row with SHOW");
                }

                Thread.sleep(1000);
                t += 1000;
            }
            if(isElementNoExist){
                return;
            }
        }

        fail("Timed out waiting: " + element);
    }


    public static void waitForElementToAppear(String element, int timeout) throws InterruptedException{
        Boolean isElementExist = false;
        while (!isElementExist) {
            int t = 0;
            while (t < timeout) {
                try {
                    //SHOW
                   /* if (driver.findElement(By.id(element)).isDisplayed()) {
                        isElementExist = true;
                        return;
                    }*/
                } catch ( StaleElementReferenceException ser ) {
                    isElementExist = false;
                } catch ( NoSuchElementException nse ) {
                    isElementExist = false;
                } catch ( Exception e ) {
                    isElementExist = false;
                    System.out.println("wait to element appear: "+element);
                }

                Thread.sleep(1000);
                t += 1000;
            }
            if(isElementExist){
                return;
            }
        }

        fail("Timed out waiting: " + element);
    }

}
