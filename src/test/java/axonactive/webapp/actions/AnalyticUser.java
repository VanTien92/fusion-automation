package axonactive.webapp.actions;

import static org.junit.Assert.*;

import axonactive.core.SystemPropertiesCore;
import axonactive.dao.DataBaseUtils;
import axonactive.webapp.steps.AnalyticUserSteps;
import axonactive.webapp.steps.CommonSteps;
import axonactive.webapp.utils.TestUtils;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.AfterStory;
import org.jbehave.core.annotations.BeforeStory;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by trannhudiem on 7/31/15.
 */
public class AnalyticUser {
    @Steps
    AnalyticUserSteps analyticUserSteps;

    @Steps
    CommonSteps commonSteps;

    @BeforeStory
    public void setUpBeforeScenario() {
        if (analyticUserSteps.getDriver() != null) {
            analyticUserSteps.getDriver().manage().window().maximize();
            analyticUserSteps.getDriver().manage().deleteAllCookies();
        }
    }

    @AfterStory()
    public void finishScenario() {
        if (analyticUserSteps.getDriver() != null) {
            analyticUserSteps.getDriver().close();
        }
    }

    @Given("I go to Users page")
    public void GotoUsers() throws InterruptedException {
        analyticUserSteps.gotoUsersPage();
    }

    @Then("I generate data for test analytic user")
    public void generateDataAnalyticUser() {
        insertDataTest();
    }

    @Then("I delete data after verify number")
    public void clearDataAfterTestRun() {
        DataBaseUtils.clearDataWithAppId(SystemPropertiesCore.getProperties().getProperty("APP_ID"));
    }

    @Then("I choose Today filter")
    public void chooseTodayFilter() throws InterruptedException {
        commonSteps.filterToday();
    }

    @Then("I verify Total User $Total_Users, Total New Users $New_Users, Total Returning Users $Return_Users")
    public void VerifyTotalUser(String Total_Users, String New_Users, String Return_Users) throws InterruptedException{
        int totalUser = Integer.valueOf(Total_Users);
        int newUsers = Integer.valueOf(New_Users);
        int returnUsers = Integer.valueOf(Return_Users);
        System.out.println("HERE: " + totalUser + " - " + newUsers + " - " + returnUsers);
        assertTrue(analyticUserSteps.VerifyTotalUser(totalUser, newUsers ,returnUsers));
    }

    @Then("I search table User with value $search_value")
    public void SearchTable(String search_value) throws InterruptedException {
        analyticUserSteps.SearchTable(search_value);
    }
    @Then("I verify No User result with message $message")
    public void VerifySearchUser(String message) throws InterruptedException{
        analyticUserSteps.VerifySearchUser(message);
    }

    List<Date> timesStart = new ArrayList<Date>();
    List<Date> timesStop = new ArrayList<Date>();
    List<Date> timesDateJoined = new ArrayList<Date>();

    private void generateDate() {
        DateTime now = new DateTime(DateTimeZone.UTC);
        //User 1
        timesStart.add(new DateTime(now).withHourOfDay(3).toDate());
        timesStop.add(null);
        timesDateJoined.add(new DateTime(now).withHourOfDay(3).toDate());

        //User 2
        timesStart.add(new DateTime(now).withHourOfDay(5).withMinuteOfHour(10).toDate());
        timesStop.add(new DateTime(now).withHourOfDay(5).withMinuteOfHour(11).toDate());
        timesDateJoined.add(new DateTime(now).withHourOfDay(5).withMinuteOfHour(10).toDate());
    }

    private void insertDataTest() {
        generateDate();
        BasicDBList dbListSession = TestUtils.readJsonFile("analytic/user/session.json");
        int i = 0;
        for (Object object : dbListSession) {
            BasicDBObject dbObject = (BasicDBObject) object;
            dbObject.put("start", timesStart.get(i));
            dbObject.put("stop", timesStop.get(i));
            ((BasicDBObject) dbObject.get("user")).put("dateJoined", timesDateJoined.get(i));
            ((BasicDBObject) dbObject.get("app")).put("id", SystemPropertiesCore.getProperties().getProperty("APP_ID"));
            DataBaseUtils.insertDataToCollection(dbObject, "session");
            i++;
        }

        BasicDBList dbListUser = TestUtils.readJsonFile("analytic/user/user.json");
        i = 0;
        for (Object object : dbListUser) {
            BasicDBObject dbObject = (BasicDBObject) object;
            dbObject.put("dateJoined", timesDateJoined.get(i));
            ((BasicDBObject) dbObject.get("retAd")).put("lstSsOn", timesStart.get(i));
            ((BasicDBObject) dbObject.get("retAd")).put("preLstSs", null);
            dbObject.put("appId", SystemPropertiesCore.getProperties().getProperty("APP_ID"));
            DataBaseUtils.insertDataToCollection(dbObject, "user");
            i++;
        }
    }
}
