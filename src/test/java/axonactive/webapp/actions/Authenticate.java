package axonactive.webapp.actions;

import static org.junit.Assert.*;

import axonactive.webapp.steps.DashboardSteps;
import axonactive.webapp.steps.LoginSteps;
import axonactive.webapp.steps.LogoutSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.*;

/**
 * Created by trannhudiem on 9/10/15.
 */
public class Authenticate {
    @Steps
    LoginSteps loginSteps;

    @Steps
    LogoutSteps logoutSteps;

    @Steps
    DashboardSteps dashboardSteps;

    @BeforeStory()
    public void setUpBeforeScenario() {
        if (loginSteps.getDriver() != null) {
            loginSteps.getDriver().manage().window().maximize();
            loginSteps.getDriver().manage().deleteAllCookies();
        }
    }

    @AfterStory()
    public void finishScenario() {
        if (loginSteps.getDriver() != null) {
            loginSteps.getDriver().close();
        }
    }

    @Given("I go to login page")
    public void goToLoginPage() throws InterruptedException {
        loginSteps.openLoginPage();
    }


    @When("I enter Username $username and Password $password")
    public void infoLogin(String username, String password) throws InterruptedException {
        loginSteps.enterUserNamePassword(username, password);
    }

    @Then("I click Remember me")
    public void clickRememberMe() throws InterruptedException {
        loginSteps.clickRememberMeCheckbox();

    }

    @Then("Click on Signin button")
    public void clickSignInButton() throws InterruptedException {
        loginSteps.clickSignInButton();

    }

    @Then("I verify login unsuccessful with the warning message  $warning_messsage")
    public void verifyLoginUnsuccessful(String warning_messsage) throws InterruptedException {
        loginSteps.verifyLoginUnsuccessful(warning_messsage);
    }

    @Then("I navigate to app selected")
    public void navigateToAppSelected() throws InterruptedException {
        dashboardSteps.navigateToAppSelected();
    }

    @Then("I verify Dashboard is displayed")
    public void displayDashboard() throws InterruptedException {
        dashboardSteps.displayDashboard();
    }

    @Then("I click Log out button")
    public void clickLogoutButton() throws InterruptedException {
        logoutSteps.clickLogoutButton();
    }

    @Then("I verify User $username choose All apps to show dashboard all apps that are assigned")
    public void ChooseAllApps(String username) {
        dashboardSteps.ChooseAllApps(username);
    }

    @Then("I verify User $username choose an app assigned")
    public void ChooseAssignedApp(String username) throws InterruptedException {
        assertTrue(dashboardSteps.ChooseAssignedApp(username));
    }

    @Then("I verify User $username view Analytics")
    public void ViewAnalytics(String username) {
        dashboardSteps.ViewAnalytics(username);
    }

    @Then("I verify User $username view Engagement")
    public void ViewEngagement(String username) {
        dashboardSteps.ViewEngagement(username);
    }

    @Then("I verify User $username view Events")
    public void ViewEvents(String username) {
        dashboardSteps.ViewEvents(username);
    }

    @Then("I verify User $username view Funnel")
    public void ViewFunnel(String username) {
        dashboardSteps.ViewFunnel(username);
    }

    @Then("I verify User $username view all reports and metrics under Sources")
    public void ViewSource(String username) {
        dashboardSteps.ViewSource(username);
    }

    @Then("I verify User $username view the list and detail view of User Profiles")
    public void ViewUserProfile(String username) {
        dashboardSteps.ViewUserProfile(username);
    }

    @Then("I verify User $username view the documentations")
    public void ViewDocumentationsWithUserRole(String username) {
        dashboardSteps.ViewDocumentationsWithUserRole(username);
    }

    @Then("I verify User $username can't view all logs page")
    public void NotViewLogPage(String username) throws InterruptedException {
        dashboardSteps.NotViewLogPage(username);
    }

    @Then("I verify User $username can't view generate tracking URLs page")
    public void NotViewGenerateURL(String username) throws InterruptedException {
        dashboardSteps.NotViewGenerateURL(username);
    }

    @Then("I verify User $username can't view application page")
    public void NotViewApplicationsPage(String username) {
        dashboardSteps.NotViewApplicationsPage(username);
    }

    @Then("I verify User $username can't view Login Account page")
    public void NotViewLoginAccountPage(String username) {
        dashboardSteps.NotViewLoginAccountPage(username);
    }

    //Admin
    @Then("I verify User $username create, edit, delete Fusion users in Login Account page")
    public void LoginAccountPage(String username) throws InterruptedException {
        dashboardSteps.LoginAccountPage(username);
    }

    @Then("I verify User $username view the event logs, system logs, crashlog")
    public void ViewLogPage(String username) throws InterruptedException {
        dashboardSteps.ViewLogPage(username);
    }

    @Then("I verify User $username view, generate, copy tracking URLs")
    public void GenerateURL(String username) throws InterruptedException {
        dashboardSteps.GenerateURL(username);
    }

    @Then("I verify User $username view, create, edit, delete application")
    public void ApplicationsPage(String username) throws InterruptedException {
        dashboardSteps.ApplicationsPage(username);
    }

    @Given("I am on Fusion Dashboard")
    public void onFusion() {
        logoutSteps.onFusion();
    }

    @When("I click Log out button")
    public void clickLogout() throws InterruptedException{
        logoutSteps.clickLogout();
    }

    @Then("I verify Log in page displayed")
    public void displayLoginPage() throws InterruptedException{
        logoutSteps.displayLoginPage();
    }

}