package axonactive.webapp.actions;

import axonactive.webapp.steps.CommonSteps;
import axonactive.webapp.steps.DashboardSteps;
import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;

/**
 * Created by nvtien2 on 18/05/2016.
 */
public class VerifyDashboard {
    @Steps
    CommonSteps commonSteps;
    @Steps
    DashboardSteps dashboardSteps;

    @Given("Select application")
    public void selectApp() throws InterruptedException {
        commonSteps.selectApp();
    }

    //    @Then("I filter 24 hours")
//    public void filter24h() throws InterruptedException {
//        commonSteps.filter24h();
//    }
//
//    @Then("I filter 7 days")
//    public void filter7days() throws InterruptedException {
//        commonSteps.filter7days();
//    }
//
//    @Then("I filter 1 Month")
//    public void filter1Month() throws InterruptedException {
//        commonSteps.filter1Month();
//    }

    @Then("I filter Custom day")
    public void filterCustom() throws InterruptedException {
        commonSteps.filterCustom();

    }

    @Then("I click on Filter Country")
    public void ClickCountry() throws InterruptedException {
        commonSteps.ClickCountry();
    }


    @Then("I click on Filter Login Type")
    public void ClickLoginType() throws InterruptedException {
        commonSteps.ClickLoginType();
    }

    @Then("I verify data of Dashboard Total Users $TotalUsers, Total New Users $NewUsers,Total Sessions $TotalSessions")
    public void VerifyDataOfCustomDay(Integer TotalUsers, Integer NewUsers, Integer TotalSessions) throws InterruptedException {
        commonSteps.VerifyDataOfCustomDay(TotalUsers, NewUsers, TotalSessions);
        Thread.sleep(5000);
    }

}
