package axonactive.webapp.utils;

/**
 * Created by nvtien2 on 19/05/2016.
 */
public class Constant {
    public static String LOGIN_URL = "/login";
    public static String ANALYTIC_USER_URL = "/analytic/users";
}
