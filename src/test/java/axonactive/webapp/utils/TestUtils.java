package axonactive.webapp.utils;

import axonactive.core.SystemPropertiesCore;
import axonactive.dao.DataBaseUtils;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;
import java.util.Properties;

/**
 * Created by trannhudiem on 7/31/15.
 */
public class TestUtils {

	public static Properties readUserConfigs(InputStream input) {
		Properties prop = new Properties();
		try {
			prop.load(input);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return prop;
	}

	public static BasicDBList readJsonFile(String path) {
		BasicDBList basicDBList = new BasicDBList();
		try {
			URL realPath = SystemPropertiesCore.loadFileFromResource(path);
			if (realPath != null) {
				FileReader fileReader = new FileReader(realPath.getFile());
				JSONParser parser = new JSONParser();
				JSONArray jsonObject = (JSONArray) parser.parse(fileReader);
				basicDBList = (BasicDBList) com.mongodb.util.JSON.parse(jsonObject.toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return basicDBList;
	}
}
