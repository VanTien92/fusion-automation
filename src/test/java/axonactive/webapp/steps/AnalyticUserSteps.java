package axonactive.webapp.steps;

import axonactive.core.SystemPropertiesCore;
import axonactive.webapp.pages.AnalyticUsersPage;
import axonactive.webapp.utils.Constant;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

/**
 * Created by trannhudiem on 10/7/15.
 */
public class AnalyticUserSteps extends ScenarioSteps {

    AnalyticUsersPage analyticUsersPage;
    //------------- ANALYTIC USER

    @Step
    public void gotoUsersPage() throws InterruptedException {
        analyticUsersPage.gotoUsersPage();
    }

    @Step
    public boolean VerifyTotalUser(int totalUserExpected, int newUserExpected, int returnUserExpected) throws InterruptedException{
        return analyticUsersPage.VerifyTotalUser(totalUserExpected, newUserExpected, returnUserExpected);
    }
    @Step
    public void SortTable() throws InterruptedException {
        analyticUsersPage.SortTable();
    }
    @Step
    public void VerifySortUser(String second_date, String first_date) throws InterruptedException{
        analyticUsersPage.VerifySortUser(second_date,first_date);
    }
    @Step
    public void SearchTable(String value) throws InterruptedException {
        analyticUsersPage.SearchTable(value);
    }
    @Step
    public void VerifySearchUser(String message) throws InterruptedException{
        analyticUsersPage.VerifySearchUser(message);
    }


}
