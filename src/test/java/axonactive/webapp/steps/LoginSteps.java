package axonactive.webapp.steps;

import axonactive.core.SystemPropertiesCore;
import axonactive.webapp.pages.LoginPage;
import axonactive.webapp.utils.Constant;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;

/**
 * Created by nvtien2 on 18/05/2016.
 */
public class LoginSteps extends PageObject {
    LoginPage loginPage;

    @Step
    public void openLoginPage() throws InterruptedException {
        loginPage.setDefaultBaseUrl(SystemPropertiesCore.getProperties().getProperty("DASHBOARD_URL") + Constant.LOGIN_URL);
        loginPage.open();
        Thread.sleep(300);
    }

    @Step
    public void enterUserNamePassword(String username, String password) throws InterruptedException{
        loginPage.enterUserNamePassword(username, password);
    }

    @Step
    public void clickSignInButton() throws InterruptedException {
        loginPage.clickSignInButton();
    }

    @Step
    public void clickRememberMeCheckbox() throws InterruptedException {
        loginPage.clickRememberMeCheckbox();
    }
    @Step
    public void verifyLoginUnsuccessful(String warning_messsage) throws InterruptedException {
        loginPage.verifyLoginUnsuccessful(warning_messsage);
    }
}
