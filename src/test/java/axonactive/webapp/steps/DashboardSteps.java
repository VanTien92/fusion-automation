package axonactive.webapp.steps;

import axonactive.webapp.pages.DashboardPage;
import axonactive.webapp.pages.LogoutPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.PageObject;
import net.thucydides.core.steps.ScenarioSteps;

/**
 * Created by trannhudiem on 12/2/15.
 */
public class DashboardSteps extends ScenarioSteps{
    DashboardPage dashboardPage;

    @Step
    public void navigateToAppSelected() throws InterruptedException{
        dashboardPage.openDashboardWithAppName();
    }
    @Step
    public void displayDashboard() throws InterruptedException{
        dashboardPage.displayDashboard();
    }
    @Step
    public void ViewLogPage(String username) throws InterruptedException {
        dashboardPage.ViewLogPage(username);
    }
    @Step
    public void GenerateURL(String username) throws InterruptedException {
        dashboardPage.GenerateURL(username);
    }
    @Step
    public void ApplicationsPage(String username) throws InterruptedException {
        dashboardPage.ApplicationsPage(username);
    }

    @Step
    public void LoginAccountPage(String username) throws InterruptedException {
        dashboardPage.LoginAccountPage(username);
    }

    @Step
    public void ViewDocumentationsWithUserRole(String username) {
        dashboardPage.ViewDocumentationsWithUserRole(username);
    }

    @Step
    public void ViewDocumentationsWithAdminRole(String username) {
        dashboardPage.ViewDocumentationsWithAdminRole(username);
    }

    @Step
    public void ViewUserProfile(String username) {
        dashboardPage.ViewUserProfile(username);
    }

    @Step
    public void ViewSource(String username) {
        dashboardPage.ViewSource(username);
    }

    @Step
    public void ViewFunnel(String username) {
        dashboardPage.ViewFunnel(username);
    }

    @Step
    public void ViewEvents(String username) {
        dashboardPage.ViewEvents(username);
    }

    @Step
    public void ViewEngagement(String username) {
        dashboardPage.ViewEngagement(username);
    }

    @Step
    public void ViewAnalytics(String username) {
        dashboardPage.ViewAnalytics(username);
    }

    @Step
    public void ChooseAllApps(String username) {
        dashboardPage.ChooseAllApps(username);
    }

    @Step
    public boolean ChooseAssignedApp(String username) throws InterruptedException {
        return dashboardPage.ChooseAssignedApp(username);
    }

    @Step
    public void NotViewLogPage(String username) throws InterruptedException {
        dashboardPage.NotViewLogPage(username);
    }
    @Step
    public void NotViewGenerateURL(String username) throws InterruptedException{
        dashboardPage.NotViewGenerateURL(username);
    }
    @Step
    public void NotViewApplicationsPage(String username){
        dashboardPage.NotViewApplicationsPage(username);
    }

    @Step
    public void NotViewLoginAccountPage(String username){
        dashboardPage.NotViewLoginAccountPage(username);
    }
}
