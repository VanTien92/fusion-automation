package axonactive.webapp.steps;

import axonactive.webapp.pages.LogoutPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;

/**
 * Created by nvtien2 on 18/05/2016.
 */
public class LogoutSteps extends PageObject {
    LogoutPage logoutPage;

    @Step
    public void clickLogoutButton() throws InterruptedException {
        logoutPage.clickLogout();
        Thread.sleep(300);
    }

    @Step
    public void onFusion() {
        logoutPage.onFusion();
    }

    @Step
    public void clickLogout() throws InterruptedException{
        logoutPage.clickLogout();
    }

    @Step
    public void displayLoginPage() throws InterruptedException{
        logoutPage.displayLoginPage();
    }
}
