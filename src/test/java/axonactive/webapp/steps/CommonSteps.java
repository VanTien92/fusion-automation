package axonactive.webapp.steps;

import axonactive.webapp.pages.Common.FilterByTime;
import axonactive.webapp.pages.Common.FilterCountry;
import axonactive.webapp.pages.Common.FilterLoginType;
import axonactive.webapp.pages.DashboardPage;
import axonactive.webapp.pages.LogoutPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;

/**
 * Created by trannhudiem on 8/14/15.
 */
public class CommonSteps extends PageObject {
    FilterByTime filterByTime;
    FilterCountry filterCountry;
    FilterLoginType filterLoginType;
    DashboardPage dashboardPage;

    //--------------- SELECT APP
    @Step
    public void selectApp() throws InterruptedException {
        dashboardPage.selectApp();
    }

    //--------------- FILTER BY TIME
    @Step
    public void filterToday() throws InterruptedException {
        filterByTime.filterToday();
    }

    @Step
    public void filter24h() throws InterruptedException {
        filterByTime.filter24h();
    }

    @Step
    public void filter7days() throws InterruptedException {
        filterByTime.filter7days();
    }

    @Step
    public void filter1Month() throws InterruptedException {
        filterByTime.filter1Month();
    }

    @Step
    public void filterCustom() throws InterruptedException {
        filterByTime.filterCustom();
    }

    //--------------- FILTER COUTRY
    @Step
    public void ClickCountry() throws InterruptedException {
        filterCountry.ClickCountry();
    }

    //--------------- FILTER LOGIN TYPE
    @Step
    public void ClickLoginType() throws InterruptedException {
        filterLoginType.ClickLoginType();
    }

    @Step
    public void VerifyDataOfCustomDay(Integer TotalUsers, Integer NewUsers, Integer TotalSessions) throws InterruptedException {
        filterByTime.VerifyDataOfCustom(TotalUsers, NewUsers, TotalSessions);
    }
}