package axonactive.webapp.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

/**
 * Created by trannhudiem on 7/31/15.
 */

public class AnalyticUsersPage extends PageObject{
    @FindBy(xpath = ".//*[@id='side-menu']/li[5]/a/span[1]")
    WebElementFacade menuAnalytic;
    @FindBy(xpath = ".//*[@id='side-menu']/li[5]/ul/li[1]/a")
    WebElementFacade usersMenu;
    @FindBy (xpath = ".//*[@id='total-all-user']")
    WebElementFacade TotalUsertxt;
    @FindBy(xpath = ".//*[@id='total-new-user']")
    WebElementFacade TotalNewUsertxt;
    @FindBy(xpath = ".//*[@id='total-return-user']")
    WebElementFacade TotalReturnUsertxt;
    @FindBy(xpath = ".//*[@id='table-data']/thead/tr/th[1]")
    WebElementFacade Time;
    @FindBy(xpath = ".//*[@id='table-data']/tbody/tr[1]/td[1]")
    WebElementFacade rowTime1;
    @FindBy(xpath = ".//*[@id='table-data_filter']/label/input")
    WebElementFacade Search;
    @FindBy(xpath = ".//*[@id='table-data']/tbody/tr/td")
    WebElementFacade messageNoData;

    @Override
    public void setDefaultBaseUrl(String defaultBaseUrl) {
        super.setDefaultBaseUrl(defaultBaseUrl);
    }

    public void gotoUsersPage() throws InterruptedException {
        menuAnalytic.waitUntilVisible().waitUntilEnabled();
        menuAnalytic.click();
        Thread.sleep(1000);
        usersMenu.waitUntilVisible().waitUntilEnabled();
        usersMenu.click();
        Thread.sleep(5000);
    }


    public boolean VerifyTotalUser(int totalUserExpected, int newUserExpected, int returnUserExpected) throws InterruptedException{
        int analyticTotaluser;
        int analyticTotalNewUser;
        int analyticReturnUser;

        TotalUsertxt.waitUntilVisible().waitUntilEnabled();
        analyticTotaluser=Integer.valueOf(TotalUsertxt.getText().trim());

        TotalNewUsertxt.waitUntilVisible().waitUntilEnabled();
        analyticTotalNewUser=Integer.valueOf(TotalNewUsertxt.getText().trim());

        TotalReturnUsertxt.waitUntilVisible().waitUntilEnabled();
        analyticReturnUser=Integer.valueOf(TotalReturnUsertxt.getText().trim());

        System.out.println("HERE 2: " + analyticTotaluser + " - " + analyticTotalNewUser + " - " + analyticReturnUser);
        return ((analyticTotaluser == totalUserExpected) && (analyticTotalNewUser == newUserExpected) && (analyticReturnUser == returnUserExpected));
    }

    public void SortTable() throws InterruptedException {
        Time.waitUntilVisible().waitUntilEnabled();
        Time.click();
        Thread.sleep(5000);
    }

    public boolean VerifySortUser(String second_date, String first_date) throws InterruptedException{
        rowTime1.waitUntilVisible().waitUntilEnabled();
        if(rowTime1.getText().equals(second_date)){
            return true;
        }return false;
    }


    public void SearchTable(String value) throws InterruptedException {
        Search.waitUntilVisible().waitUntilEnabled();
        Search.type(value);

    }
    public boolean VerifySearchUser(String message) throws InterruptedException{
        if(messageNoData.getText().equals(message)){
            Thread.sleep(2000);
            return true;
        }return false;

    }
}