package axonactive.webapp.pages;

import axonactive.core.SystemPropertiesCore;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;

/**
 * Created by trannhudiem on 12/2/15.
 */
public class DashboardPage extends PageObject {
    @FindBy(xpath = ".//*[@id='btnAppSelect']")
    WebElementFacade btnSelectApp;
    @FindBy(xpath = ".//*[@id='55fa5c6ce4b0b0c0879a6fa2']/span[2]")
    WebElementFacade appMap;
    @FindBy(xpath = ".//*[@id='side-menu']/li[11]/a/span[1]")
    WebElementFacade DocumentMenuRoleUser;
    @FindBy(xpath = ".//*[@id='side-menu']/li[12]/a/span[1]")
    WebElementFacade DocumentMenuRoleAdmin;
    @FindBy(xpath = ".//*[@id='side-menu']/li[11]/a/span[1]")
    WebElementFacade ManagementMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[10]/a/span")
    WebElementFacade UserProfileMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[9]/a/span[1]")
    WebElementFacade SourceMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[8]/a/span")
    WebElementFacade FunnelMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[7]/a/span[1]")
    WebElementFacade EventsMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[6]/a/span[1]")
    WebElementFacade EngagementMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[5]/a/span[1]")
    WebElementFacade AnalyticMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[11]/ul/li[1]/a")
    WebElementFacade ApplicationsMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[11]/ul/li[2]/a")
    WebElementFacade LoginAccountMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[11]/ul/li[3]/a")
    WebElementFacade GenerateURLMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[11]/ul/li[4]/a")
    WebElementFacade EventLogMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[11]/ul/li[5]/a")
    WebElementFacade SystemLogMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[11]/ul/li[6]/a")
    WebElementFacade CrashlogMenu;
    @FindBy(xpath = ".//*[@id='side-menu']/li[11]/ul/li[7]/a")
    WebElementFacade UserCohortMenu;
    @FindBy(xpath = ".//*[@id='source-url-input']")
    WebElementFacade SourceURL;
    @FindBy(xpath = ".//*[@id='source-id-input']")
    WebElementFacade SourceId;
    @FindBy(xpath = ".//*[@id='btnAppSelect']")
    WebElementFacade applicationbtn;
    @FindBy(xpath = ".//*[@id='allapps']")
    WebElementFacade AllApps;
    @FindBy(xpath = ".//*[@id='filter-total-sessions']/a")
    WebElementFacade Sessiontab;

    @Override
    public void setDefaultBaseUrl(String defaultBaseUrl) {
        super.setDefaultBaseUrl(defaultBaseUrl);
    }

    public void selectApp() throws InterruptedException{
        btnSelectApp.waitUntilVisible().waitUntilEnabled();
        btnSelectApp.click();
        appMap.waitUntilVisible().waitUntilEnabled();
        appMap.click();
    }

    public boolean displayDashboard() throws InterruptedException {
        btnSelectApp.waitUntilVisible().waitUntilEnabled();
        return btnSelectApp.isDisplayed();
    }

    public boolean openDashboardWithAppName() throws InterruptedException {
        String dashboard = SystemPropertiesCore.getProperties().getProperty("DASHBOARD_URL");
        String appName = SystemPropertiesCore.getProperties().getProperty("APP_NAME_URL");
        String url = dashboard + "/app/" + appName;
        this.setDefaultBaseUrl(url);
        this.open();
        btnSelectApp.waitUntilVisible().waitUntilEnabled();
        return btnSelectApp.isDisplayed();
    }

    public boolean ViewLogPage(String username) throws InterruptedException {
        ManagementMenu.waitUntilVisible().waitUntilEnabled();
        ManagementMenu.click();
        Thread.sleep(1000);
        boolean result = (EventLogMenu.isDisplayed() && SystemLogMenu.isDisplayed() && CrashlogMenu.isDisplayed());
        ManagementMenu.click();
        return result;
    }


    public boolean GenerateURL(String username) throws InterruptedException {
        ManagementMenu.waitUntilVisible().waitUntilEnabled();
        ManagementMenu.click();
        Thread.sleep(1000);
        boolean result = GenerateURLMenu.isDisplayed();
        ManagementMenu.click();
        return result;
    }

    public boolean ApplicationsPage(String username) throws InterruptedException {
        ManagementMenu.waitUntilVisible().waitUntilEnabled();
        ManagementMenu.click();
        Thread.sleep(1000);
        boolean result = ApplicationsMenu.isDisplayed();
        ManagementMenu.click();
        return result;
    }

    public boolean LoginAccountPage(String username) throws InterruptedException {
        ManagementMenu.waitUntilVisible().waitUntilEnabled();
        ManagementMenu.click();
        Thread.sleep(1000);
        boolean result = LoginAccountMenu.isDisplayed();
        ManagementMenu.click();
        return result;
    }


    public boolean NotViewLogPage(String username) throws InterruptedException {
        if (!ManagementMenu.isDisplayed() && !EventLogMenu.isDisplayed() && !SystemLogMenu.isDisplayed() && !CrashlogMenu.isDisplayed()) {
            return true;
        }
        return false;
    }

    public boolean NotViewGenerateURL(String username) throws InterruptedException {
        if (!ManagementMenu.isDisplayed() && !GenerateURLMenu.isDisplayed()) {
            return true;
        }
        return false;
    }

    public boolean NotViewApplicationsPage(String username) {
        if (!ManagementMenu.isDisplayed() && !ApplicationsMenu.isDisplayed()) {
            return true;
        }
        return false;
    }

    public boolean NotViewLoginAccountPage(String username) {
        if (!ManagementMenu.isDisplayed() && !LoginAccountMenu.isDisplayed()) {
            return true;
        }
        return false;

    }

    public boolean ViewDocumentationsWithUserRole(String username) {
        DocumentMenuRoleUser.waitUntilVisible().waitUntilEnabled();
        return DocumentMenuRoleUser.isDisplayed();
    }

    public boolean ViewDocumentationsWithAdminRole(String username) {
        DocumentMenuRoleAdmin.waitUntilVisible().waitUntilEnabled();
        return DocumentMenuRoleAdmin.isDisplayed();
    }


    public boolean ViewUserProfile(String username) {
        UserProfileMenu.waitUntilVisible().waitUntilEnabled();
        return UserProfileMenu.isDisplayed();

    }


    public boolean ViewSource(String username) {
        SourceMenu.waitUntilVisible().waitUntilEnabled();
        return SourceMenu.isDisplayed();
    }


    public boolean ViewFunnel(String username) {
        FunnelMenu.waitUntilVisible().waitUntilEnabled();
        return FunnelMenu.isDisplayed();
    }


    public boolean ViewEvents(String username) {
        EventsMenu.waitUntilVisible().waitUntilEnabled();
        return EventsMenu.isDisplayed();
    }


    public boolean ViewEngagement(String username) {
        EngagementMenu.waitUntilVisible().waitUntilEnabled();
        return EngagementMenu.isDisplayed();
    }


    public boolean ViewAnalytics(String username) {
        AnalyticMenu.waitUntilVisible().waitUntilEnabled();
        return AnalyticMenu.isDisplayed();
    }


    public boolean ChooseAllApps(String username) {
        applicationbtn.waitUntilVisible().waitUntilEnabled();
        applicationbtn.click();
        if (AllApps.isDisplayed()) {
            AllApps.click();
            if (Sessiontab.isDisplayed()) {
                return true;
            }
            return false;
        }
        return false;
    }


    public boolean ChooseAssignedApp(String username) throws InterruptedException {
        applicationbtn.waitUntilVisible().waitUntilEnabled();
        applicationbtn.click();
        Thread.sleep(1000);
        WebElement appIdSelected = this.getDriver().findElement(By.id(SystemPropertiesCore.getProperties().getProperty("APP_ID")));
        String appName = SystemPropertiesCore.getProperties().getProperty("APP_NAME");
        appIdSelected.isDisplayed();
        appIdSelected.click();
        Thread.sleep(1000);
        return applicationbtn.getText().trim().equals(appName);
    }
}
