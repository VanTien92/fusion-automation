package axonactive.webapp.pages.Common;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

/**
 * Created by trannhudiem on 11/23/15.
 */
public class FilterLoginType extends PageObject{
    @FindBy(xpath = ".//*[@id='filter-button']")
    WebElementFacade Filterbutton;
    @FindBy(xpath = ".//*[@id='search-segment-filter']")
    WebElementFacade SearchInFilterbtn;
    @FindBy(xpath = ".//*[@id='page-wrapper']/div[2]/div[3]/div/dl/dd/div/ul/li[5]/label")
    WebElementFacade LoginType;
    //@FindBy(css = ("input[type='checkbox'][value='Unknown']"))
    @FindBy(xpath = ".//*[@id='login-type-filter']/li[4]/label")
    WebElementFacade SelectLoginType;
    @FindBy(xpath = ".//*[@id='btn-apply-filter']")
    WebElementFacade ApplyFilter;



    public void ClickLoginType() throws InterruptedException{
        Filterbutton.waitUntilVisible().waitUntilEnabled();
        Filterbutton.click();
        Thread.sleep(3000);
        SearchInFilterbtn.click();
        SearchInFilterbtn.sendKeys("Login Type");
        System.out.print("XXXX");
        Thread.sleep(2000);
        LoginType.waitUntilVisible().waitUntilEnabled();
        LoginType.click();
        Thread.sleep(3000);
        SelectLoginType.waitUntilVisible().waitUntilEnabled();
        SelectLoginType.click();
        Thread.sleep(5000);
        ApplyFilter.waitUntilVisible().waitUntilEnabled();
        ApplyFilter.click();


    }
}

