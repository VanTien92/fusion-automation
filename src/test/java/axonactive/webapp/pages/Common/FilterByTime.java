package axonactive.webapp.pages.Common;

import net.thucydides.core.pages.PageObject;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


/**
 * Created by trannhudiem on 7/30/15.
 */

public class FilterByTime extends PageObject {

    @FindBy(id = "filter-time-24hours")
    WebElementFacade btn24h;
    @FindBy(id = "filter-time-today")
    WebElementFacade btnToday;
    @FindBy(id = "filter-time-7days")
    WebElementFacade btn7days;
    @FindBy(id = "filter-time-month")
    WebElementFacade btnMonth;
    @FindBy(xpath = ".//*[@id='filter-time-customize']")
    WebElementFacade btnCustom;
    @FindBy(xpath = ".//*[@id='totalSessions']")
    public WebElementFacade totalSessiontxt;
    @FindBy(xpath = ".//*[@id='totalUser']")
    public WebElementFacade totalUsertxt;
    @FindBy(xpath = ".//*[@id='totalNewUser']")
    public WebElementFacade totalNewUserstxt;
    @FindBy(xpath = ".//*[@id='totalSessionTime']")
    public WebElementFacade totalTimeSpent;
    @FindBy(xpath = ".//*[@id='totalSessionTimeAverage']")
    public WebElementFacade getTotalTimeSpentAvr;
    @FindBy(xpath = ".//*[@id='summary']/div[8]/div/div/h2")
    public WebElementFacade Platform;
    @FindBy(xpath = ".//*[@id='drawSecond']/div/table/thead/tr[1]/th[1]/i")
    WebElementFacade customArrow2;
    @FindBy(xpath = ".//*[@id='drawFirst']/div/table/thead/tr[1]/th[1]")
    WebElementFacade customArrow1;
    @FindBy(xpath = ".//*[@id='drawSecond']/div/table/tbody/tr[5]/td[3]")
    WebElementFacade fromdate;
    @FindBy(xpath = ".//*[@id='drawFirst']/div/table/tbody/tr[5]/td[4]")
    WebElementFacade todate;
    @FindBy(xpath = "/html/body/div[16]/div[3]/div[1]/button[1]")
    WebElementFacade Applybtn;

    public void filterToday() throws InterruptedException {
        btnToday.waitUntilVisible().waitUntilEnabled();
        btnToday.click();
        Thread.sleep(2000);
    }

    public void filter24h() throws InterruptedException {
        btn24h.waitUntilVisible().waitUntilEnabled();
        btn24h.click();
        Thread.sleep(2000);
    }

    public void filter7days() throws InterruptedException {
        btn7days.waitUntilVisible().waitUntilEnabled();
        btn7days.click();
        Thread.sleep(2000);

    }

    public void filter1Month() throws InterruptedException {
//        int sessionCountsMonth = 0;
//        int m=108;
//        btnMonth.waitUntilVisible().waitUntilEnabled();
//        btnMonth.click();
//        totalSession.waitUntilVisible().waitUntilEnabled();
//        if(totalSession.getText() != null){
//            sessionCountsMonth=Integer.valueOf(totalSession.getText().toString().trim());
//        }
//        System.out.print("\n  ****************************  \n");
//        System.out.println("Total session of Month is " + sessionCountsMonth);
//        if(sessionCountsMonth == m){ System.out.print("Total sessions of Month is Right    \n");
//        }else System.out.print("Total sessions of Month is Wrong    ");
//        System.out.print("\n  ****************************  \n");
//        Thread.sleep(3000);
    }

    public void filterCustom() throws InterruptedException {

        btnCustom.waitUntilVisible().waitUntilEnabled();
        btnCustom.click();
        customArrow2.waitUntilVisible().waitUntilEnabled();
        customArrow2.click();
        fromdate.waitUntilVisible().waitUntilEnabled();
        fromdate.click();
        customArrow1.waitUntilVisible().waitUntilEnabled();
        customArrow1.click();
        todate.waitUntilVisible().waitUntilEnabled();
        todate.click();
        Applybtn.waitUntilVisible().waitUntilEnabled();
        Applybtn.click();


    }

    public boolean VerifyDataOfCustom(Integer TotalUsers, Integer NewUsers, Integer TotalSessions) throws InterruptedException {
        int sessionCountsCustom = 0;
        int userCountsCustom = 0;
        int newuserCountsCustom = 0;

        userCountsCustom = Integer.valueOf(totalUsertxt.getText().toString().trim());
        newuserCountsCustom = Integer.valueOf(totalNewUserstxt.getText().toString().trim());
        sessionCountsCustom = Integer.valueOf(totalSessiontxt.getText().toString().trim());

        if (userCountsCustom == TotalUsers) {
            System.out.print("Total users of Custom date is Right    \n");

            return true;

        } else {
            if (newuserCountsCustom == NewUsers) {
                System.out.print("Total new users of Custom date is Right    \n");
                return true;
            } else {
                if (sessionCountsCustom == TotalSessions) {
                    System.out.print("Total sessions of Custom date is Right    \n");
                    return true;
                }
            }
            return false;


        }
    }
}


