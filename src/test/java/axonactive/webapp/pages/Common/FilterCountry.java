package axonactive.webapp.pages.Common;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;

/**
 * Created by trannhudiem on 8/14/15.
 */
public class FilterCountry extends PageObject {
    @FindBy(xpath = ".//*[@id='filter-button']")
    WebElementFacade Filterbutton;
    @FindBy(xpath = ".//*[@id='search-segment-filter']")
    WebElementFacade SearchInFilterbtn;
    @FindBy(xpath = ".//*[@id='page-wrapper']/div[2]/div[3]/div/dl/dd/div/ul/li[4]/label")
    WebElementFacade Country;
    @FindBy(css = ("input[type='checkbox'][value='VN']"))
    WebElementFacade SelectCountry;
    @FindBy(xpath = ".//*[@id='btn-apply-filter']")
    WebElementFacade ApplyFilter;


    public void ClickCountry() throws InterruptedException{
        Filterbutton.waitUntilVisible().waitUntilEnabled();
        Filterbutton.click();
        Thread.sleep(3000);
        SearchInFilterbtn.click();
        SearchInFilterbtn.sendKeys("Country");
        System.out.print("XXXX");
        Thread.sleep(2000);
        Country.waitUntilVisible().waitUntilEnabled();
        Country.click();
        Thread.sleep(3000);
        System.out.print("XXXX");
        SelectCountry.waitUntilVisible().waitUntilEnabled();
        SelectCountry.click();
        Thread.sleep(5000);
        ApplyFilter.waitUntilVisible().waitUntilEnabled();
        ApplyFilter.click();


    }

}