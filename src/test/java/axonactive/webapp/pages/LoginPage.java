package axonactive.webapp.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.WebElement;
import org.reflections.vfs.Vfs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by trannhudiem on 7/31/15.
 */

public class LoginPage extends PageObject {
    @FindBy(name = "username")
    WebElementFacade username;
    @FindBy(name = "password")
    WebElementFacade password;
    @FindBy(className = "icheckbox_square-green")
    WebElementFacade rememberMe;
    @FindBy(className = "login-btn")
    WebElementFacade btnSignIn;
    @FindBy(xpath = ".//*[@id='box-login']/small")
    WebElementFacade messWarningLogin;

    @Override
    public void setDefaultBaseUrl(String defaultBaseUrl) {
        super.setDefaultBaseUrl(defaultBaseUrl);
    }

    public void enterUserNamePassword(String usernameTxt, String passwordTxt) throws InterruptedException{
        username.waitUntilVisible().waitUntilEnabled();
        password.waitUntilVisible().waitUntilEnabled();
        username.click();
        username.type(usernameTxt);
        password.click();
        password.type(passwordTxt);
        Thread.sleep(3000);

    }

    public void clickSignInButton() throws InterruptedException{
        btnSignIn.waitUntilVisible().waitUntilEnabled();
        btnSignIn.click();
        Thread.sleep(2000);

    }

    public void clickRememberMeCheckbox() throws InterruptedException{
        rememberMe.waitUntilVisible().waitUntilEnabled();
        rememberMe.click();
        Thread.sleep(2000);
    }

    public boolean verifyLoginUnsuccessful(String Warning_messsage) throws InterruptedException{
        if(messWarningLogin.getText().equals(Warning_messsage)){
            Thread.sleep(2000);
            return true;
        }
        return false;
    }
}