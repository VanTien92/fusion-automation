package axonactive.webapp.pages;

import javafx.beans.value.WeakChangeListener;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

/**
 * Created by trannhudiem on 12/2/15.
 */
public class LogoutPage extends PageObject {

    @FindBy(className = "sign-out-button")
    WebElementFacade btnLogout;

    @FindBy(xpath = ".//*[@id='wrapper']/div/div[2]/div/img")
    WebElementFacade logoFusion;

    public void onFusion() {

    }

    public void clickLogout() throws InterruptedException{
        btnLogout.waitUntilVisible().waitUntilEnabled();
        btnLogout.click();
        Thread.sleep(2000);
    }


    public boolean displayLoginPage() throws InterruptedException{
        if (logoFusion.isDisplayed()){
            Thread.sleep(3000);
            return true;
        }
        return false;
    }
}