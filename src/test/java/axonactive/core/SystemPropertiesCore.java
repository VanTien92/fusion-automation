package axonactive.core;

import axonactive.webapp.utils.TestUtils;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.webdriver.SupportedWebDriver;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;


public class SystemPropertiesCore extends net.thucydides.core.webdriver.SystemPropertiesConfiguration {

    private static Properties properties = null;

    public SystemPropertiesCore(EnvironmentVariables environmentVariables) {
        super(environmentVariables);
    }

    public SupportedWebDriver getDriverType() {
        SupportedWebDriver custom_driver = super.getDriverType();

        return custom_driver;
    }

    public static Properties getProperties() {
        if (properties == null) {
            loadFileProperties();
        }
        return properties;
    }

    public static void loadFileProperties() {
        try {
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            properties = TestUtils.readUserConfigs(classLoader.getResourceAsStream("serenity.properties"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static URL loadFileFromResource(String path) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return classLoader.getResource(path);
    }
}
