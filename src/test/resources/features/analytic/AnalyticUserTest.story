Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: ADMIN LOGIN TO THE DASHBOARD SUCCESSFUL
Given I go to login page
When I enter Username <username> and Password <password>
Then I click Remember me
Then Click on Signin button
Then I navigate to app selected
Then I verify Dashboard is displayed
Then I generate data for test analytic user
Examples:
|username           |password|
|admin.automation   |diem123|

Scenario: VERIFICATION THE PAGE ANALYTIC / USERS
Given I go to Users page
Then I choose Today filter
Then I verify Total User <Total_Users>, Total New Users <New_Users>, Total Returning Users <Return_Users>
Then I search table User with value <search_value>
Then I verify No User result with message <message>
Examples:
|Total_Users    |New_Users  |Return_Users   |search_value   |message|
|2              |2          |0              |23             |No matching records found|

Scenario: LOGOUT AFTER VERIFY
Given I am on Fusion Dashboard
When I click Log out button
Then I verify Log in page displayed
Then I delete data after verify number