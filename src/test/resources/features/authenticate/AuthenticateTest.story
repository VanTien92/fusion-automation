Meta:
Narrative:
As a metric user
I want to login in the Fusion dashboard and then data test data of app Map Demo


Scenario: LOGIN TO THE DASHBOARD UNSUCCESSFUL
Given I go to login page
When I enter Username <username> and Password <password>
Then I click Remember me
Then Click on Signin button
Then I verify login unsuccessful with the warning message  <warning_messsage>
Examples:
|username       |password           |warning_messsage|
|diem.tran.290  |diem.tran          |Invalid username and password!|
|diem.tran      |diem.tran1234567   |Invalid username and password!|


Scenario: USER LOGIN TO THE DASHBOARD SUCCESSFUL
Given I go to login page
When I enter Username <username> and Password <password>
Then I click Remember me
Then Click on Signin button
Then I navigate to app selected
Then I verify Dashboard is displayed
Then I verify User <username> choose All apps to show dashboard all apps that are assigned
Then I verify User <username> choose an app assigned
Then I verify User <username> view Analytics
Then I verify User <username> view Engagement
Then I verify User <username> view Events
Then I verify User <username> view Funnel
Then I verify User <username> view all reports and metrics under Sources
Then I verify User <username> view the list and detail view of User Profiles
Then I verify User <username> view the documentations
Then I verify User <username> can't view all logs page
Then I verify User <username> can't view generate tracking URLs page
Then I verify User <username> can't view application page
Then I verify User <username> can't view Login Account page
Then I click Log out button
Examples:
|username       |password|
|user.automation|diem456|


Scenario: ADMIN LOGIN TO THE DASHBOARD SUCCESSFUL
Given I go to login page
When I enter Username <username> and Password <password>
Then I click Remember me
Then Click on Signin button
Then I navigate to app selected
Then I verify Dashboard is displayed
Then I verify User <username> choose All apps to show dashboard all apps that are assigned
Then I verify User <username> choose an app assigned
Then I verify User <username> create, edit, delete Fusion users in Login Account page
Then I verify User <username> view Analytics
Then I verify User <username> view Engagement
Then I verify User <username> view Events
Then I verify User <username> view Funnel
Then I verify User <username> view all reports and metrics under Sources
Then I verify User <username> view the list and detail view of User Profiles
Then I verify User <username> view the event logs, system logs, crashlog
Then I verify User <username> view, generate, copy tracking URLs
Then I verify User <username> view, create, edit, delete application
Then I verify User <username> view the documentations
Examples:
|username           |password|
|admin.automation   |diem123|


Scenario: LOG OUT SUCCESSFUL
Given I am on Fusion Dashboard
When I click Log out button
Then I verify Log in page displayed